package model

import (
	"math/big"
)

type EthModel struct {
	EthValue       *big.Float
	PendingBalance *big.Int
}
