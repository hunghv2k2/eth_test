package bootstrap

import (
	"github.com/go-playground/validator/v10"
	"gitlab.com/backend/service/public/adapter"
	"gitlab.com/backend/service/public/clients"
	"gitlab.com/backend/service/public/controllers"
	"gitlab.com/backend/service/public/properties"
	"gitlab.com/backend/service/public/routers"
	"gitlab.com/golibs-starter/golib"
	golibgin "gitlab.com/golibs-starter/golib-gin"
	"go.uber.org/fx"
)

// All register all constructors for fx container
func All() fx.Option {
	return fx.Options(
		golib.AppOpt(),
		golib.PropertiesOpt(),
		golib.LoggingOpt(),
		golib.EventOpt(),
		golib.BuildInfoOpt(Version, CommitHash, BuildTime),
		golib.ActuatorEndpointOpt(),
		golib.ProvideProps(properties.NewSwaggerProperties),
		golib.ProvideProps(properties.NewSubmitProperties),
		golibgin.GinHttpServerOpt(),

		fx.Provide(adapter.NewEvmAdapter),
		fx.Provide(clients.NewEthClient),
		fx.Provide(controllers.NewEthController),
		fx.Provide(validator.New),

		fx.Invoke(routers.RegisterHandlers),
		fx.Invoke(routers.RegisterGinRouters),

		// Graceful shutdown.
		// OnStop hooks will run in reverse order.
		// golib.OnStopEventOpt() MUST be first
		golib.OnStopEventOpt(),
		golibgin.OnStopHttpServerOpt(),
	)
}
