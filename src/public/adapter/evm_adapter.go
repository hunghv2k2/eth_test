package adapter

import (
	"context"
	"errors"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/backend/service/public/model"
	"gitlab.com/backend/service/public/properties"
	"math"
	"math/big"
)

type EvmAdapter struct {
	ethClient        *ethclient.Client
	submitProperties *properties.SubmitProperties
}

func NewEvmAdapter(
	ethClient *ethclient.Client, submitProperties *properties.SubmitProperties,
) BlockchainPort {
	return &EvmAdapter{
		ethClient:        ethClient,
		submitProperties: submitProperties,
	}
}

func (e *EvmAdapter) Transfer(recipient string) (*model.EthModel, error) {
	var data model.EthModel
	account := common.HexToAddress(recipient)
	balance, err := e.ethClient.BalanceAt(context.Background(), account, nil)
	if err != nil {
		return nil, errors.New("error when get balance")
	}

	if balance.Cmp(big.NewInt(0)) == -1 {
		return nil, errors.New("not enough balance")
	}

	fbalance := new(big.Float)
	fbalance.SetString(balance.String())
	ethValue := new(big.Float).Quo(fbalance, big.NewFloat(math.Pow10(18)))
	data.EthValue = ethValue

	pendingBalance, err := e.ethClient.PendingBalanceAt(context.Background(), account)
	if err != nil {
		return nil, err
	}
	data.PendingBalance = pendingBalance
	return &data, nil
}
