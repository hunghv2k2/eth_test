package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/backend/service/public/adapter"
	"gitlab.com/backend/service/public/request"
	"gitlab.com/backend/service/public/resources"
	baseEx "gitlab.com/golibs-starter/golib/exception"
	"gitlab.com/golibs-starter/golib/log"
	"gitlab.com/golibs-starter/golib/web/response"
	"net/http"
)

type EthController struct {
	blockchainPort adapter.BlockchainPort
	validator      *validator.Validate
}

func NewEthController(
	blockchainPort adapter.BlockchainPort, validator *validator.Validate,
) *EthController {
	return &EthController{
		blockchainPort: blockchainPort,
		validator:      validator,
	}
}

func (c *EthController) Transfer(ctx *gin.Context) {
	var req request.AddressRequest
	if err := ctx.BindQuery(&req); err != nil {
		validationErrors, ok := err.(validator.ValidationErrors)
		if ok {
			response.WriteError(ctx.Writer, baseEx.New(http.StatusBadRequest, validationErrors[0].Error()))
			return
		}
		log.Warn(ctx, "[GetByAddress] cannot bind request error: [%s]", err)
		response.WriteError(ctx.Writer, baseEx.BadRequest)
		return
	}

	data, err := c.blockchainPort.Transfer(req.Address)
	if err != nil {
		log.Error(ctx, "Transfer failed, err [%v]", err)
		response.WriteError(ctx.Writer, err)
		return
	}

	response.Write(ctx.Writer, response.Ok(new(resources.EthResource).FromEntity(data)))
}
