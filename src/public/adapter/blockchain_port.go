package adapter

import "gitlab.com/backend/service/public/model"

type BlockchainPort interface {
	Transfer(string) (*model.EthModel, error)
}
