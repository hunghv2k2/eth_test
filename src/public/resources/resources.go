package resources

import (
	"gitlab.com/backend/service/public/model"
	"math/big"
)

type EthResource struct {
	EthValue       *big.Float `json:"eth_value,omitempty"`
	PendingBalance *big.Int   `json:"pending_balance,omitempty"`
}

func (e *EthResource) FromEntity(eth *model.EthModel) *EthResource {
	e.EthValue = eth.EthValue
	e.PendingBalance = eth.PendingBalance
	return e
}
