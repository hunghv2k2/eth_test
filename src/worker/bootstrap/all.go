package bootstrap

import (
	"gitlab.com/golibs-starter/golib"
	"gitlab.com/golibs-starter/golib-gin"
	"gitlab.com/backend/service/worker/properties"
	"gitlab.com/backend/service/worker/routers"
	"go.uber.org/fx"
)

// All register all constructors for fx container
func All() fx.Option {
	return fx.Options(
		golib.AppOpt(),
		golib.PropertiesOpt(),
		golib.LoggingOpt(),
		golib.EventOpt(),
		golib.BuildInfoOpt(Version, CommitHash, BuildTime),
		golib.ActuatorEndpointOpt(),
		golib.ProvideProps(properties.NewSwaggerProperties),
		golibgin.GinHttpServerOpt(),
		fx.Invoke(routers.RegisterHandlers),
		fx.Invoke(routers.RegisterGinRouters),

		// Graceful shutdown.
		// OnStop hooks will run in reverse order.
		// golib.OnStopEventOpt() MUST be first
		golib.OnStopEventOpt(),
		golibgin.OnStopHttpServerOpt(),
	)
}
