package request

type AddressRequest struct {
	Address string `form:"address" binding:"required,eth_addr"`
}
