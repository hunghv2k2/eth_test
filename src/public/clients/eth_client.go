package clients

import (
	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/backend/service/public/properties"
)

func NewEthClient(submitProps *properties.SubmitProperties) (*ethclient.Client, error) {
	client, err := ethclient.Dial(submitProps.URL)
	if err != nil {
		return nil, err
	}
	return client, nil
}
